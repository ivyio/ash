
* Ash

[[./ash.jpg]]


Ash is a Selenium client for Common Lisp.  It was written against the Selenium
2.x JSON wire protocol described [[https://w3c.github.io/webdriver/webdriver-spec.html][here]].

It is *incomplete* - This was written for a separate project and I added
functionality as I needed it.  Over time, however, it has proved to be quite
useful.


* Examples

#+NAME: setup
#+BEGIN_SRC lisp
(setf ash:*ash-host* "127.0.0.1") ;; These are the defaults.
(setf ash:*ash-port* 4444) 
#+END_SRC

#+NAME: example1
#+BEGIN_SRC lisp
(ash:with-session ()
  (ash:page-open "http://google.com"))
#+END_SRC

Creates a new session, opens the page, and closes the session.

#+NAME: example2
#+BEGIN_SRC lisp
(ash:with-session (:autoclose nil)
  (ash:page-open "http://www.google.com")
  (ash:page-url)))
#+END_SRC

Creates a new session, opens the page, but does not close the session.

#+NAME: example3
#+BEGIN_SRC lisp
(ash:with-a-session
  (ash:page-click
    (find-element-by-text "Login")))
#+END_SRC

Uses the session from example2, and clicks on the first element with the text
"Login".

Note that ~with-a-session~ will use the *first available session* with no regard
to whether it is in use or not.  It's provided to make it easier to debug your
test functions.

